# React + Redux + Unsplash API

This app consumes the unsplash api providing users the functionality to search for a username and view the found user's images.

The was generated from the `create-react-app` scaffold with no major changes.

Redux was added in to handle global application state, although not really necessary, I thought it would show my capabilities.

I leveraged css modules to keep my styling namespaced.

I also added a minified version of flexboxgrid to help with page layout.

Application flow:
  1. User lands on root page (Home component)
  2. the enter some text in the search bar
  3. they click search
  4. handler action is called
  5. a GET request is sent to the unsplash api through the axios client
  6. the data is mapped back into redux
  7. the UI is re-rendered
    1. the selectors are invoked from mapStateToProps, adding the userList into our Home components props
    2. the success and failure states are propagated down to the relevant components to update the ui
  8. if the username was not found, an error message will inform the user
  9. if the user was found, the user name will be rendered with a clickable link
  10. the users clicks the `Show Photos` button which set's the Home components state to have the photos associated with the username as the `photoGridImages` prop
  11. PhotoGrid component recieves the props, and renders the photo's into the UI

Other things I would have linked to add:
 - Jest and Enzye, the testing tools provided by create-react-app are not enough
 - Linting, every project needs it, I'm partial to the Airbnb rule set.
 - Icon sets and better styling
 - Proper footer icons
