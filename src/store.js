// @TODO: Anthony - implement routing if time permits
import { createStore } from 'redux';
import rootReducer from './reducers';

const initialState = {
  userReducer: {
    userList: [],
  },
};

export default createStore(rootReducer, initialState);
