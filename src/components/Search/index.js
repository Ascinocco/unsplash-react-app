import PropTypes from 'prop-types';
import React from 'react';
import styles from './Search.module.css';

const propTypes = {
  onChangeInput: PropTypes.func.isRequired,
  onClickSearch: PropTypes.func.isRequired,
  userNotFoundError: PropTypes.bool,
  value: PropTypes.string,
};

const defaultProps = {
  value: '',
  userNotFoundError: undefined,
};

const displayError = error => !error ? null : (
  <h5 className={styles['search-error']}>The user you searched for was not found...</h5>
);

const Search = ({ onChangeInput, onClickSearch, value, userNotFoundError }) => (
  <div>
    <h4>Find Users</h4>
    {displayError(userNotFoundError)}
    <label>
      <input
        type="text"
        name="userName"
        onChange={onChangeInput}
        value={value}
      />
      <button onClick={onClickSearch}>
        Search
      </button>
    </label>
  </div>
);

Search.propTypes = propTypes;
Search.defaultProps = defaultProps;

export default Search;
