import uuid from 'uuid/v4';
import isEmpty from 'lodash.isempty';
import chunk from 'lodash.chunk';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './PhotoGrid.module.css';

const propTypes = {
  currentlySelectedUser: PropTypes.object,
  photoGridImages: PropTypes.array,
};

const defaultProps = {
  currentlySelectedUser: {},
  photoGridImages: [],
};

const PhotoGrid = ({ currentlySelectedUser, photoGridImages }) => {
  const photoGridIsEmpty = isEmpty(photoGridImages);
  const noUserSelected = isEmpty(currentlySelectedUser);
  if (photoGridIsEmpty && noUserSelected) {
    return (
      <h4>Search for a user to get started</h4>
    );
  }

  if (photoGridIsEmpty && !noUserSelected) {
    return (
      <h4>{currentlySelectedUser.username} has no photo's to display</h4>
    );
  }

  const chunkedImages = chunk(photoGridImages, 4);
  return chunkedImages.map(chunk => (
    <div
      key={`chunked-photo-grid-row-${uuid()}`}
      className={styles['photo-grid-chunk-container']}
    >
      {chunk.map(photo => (
        <img
        className={styles['photo-grid-image']}
        key={photo.id}
        src={photo.urls.regular}
        alt={'This should include better user info'}
      />
      ))}
    </div>
  ));
}

PhotoGrid.propTypes = propTypes;
PhotoGrid.defaultProps = defaultProps;

export default PhotoGrid;
