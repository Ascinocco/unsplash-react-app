import React from 'react';
import styles from './Footer.module.css';

const PhotoGrid = () => (
  <footer className={styles['footer-container']}>
    <a href="https://angel.co/anthony-scinocco?al_content=current+user+name&al_source=transaction_feed%2Fnetwork_sidebar" target="_blank">By Anthony Scinocco</a>
  </footer>
);

export default PhotoGrid;
