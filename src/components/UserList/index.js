import PropTypes from 'prop-types';
import React from 'react';
import styles from './UserList.module.css';

const propTypes = {
  currentlySelectedUser: PropTypes.object,
  userList: PropTypes.array,
  onSelectUser: PropTypes.func.isRequired,
};

const defaultProps = {
  currentlySelectedUser: {},
  userList: [],
};

const UserList = ({ currentlySelectedUser, userList, onSelectUser }) => userList.map(user => {
  const className = currentlySelectedUser.id === user.id ?
    styles['currently-selected-user'] :
    styles['user-name'];

  return (
    <div key={user.id}>
      <span className={className}>{user.username}</span>
      <button onClick={() => onSelectUser(user)}>
        Show Photo's
      </button>
    </div>
  );
});

UserList.propTypes = propTypes;
UserList.defaultProps = defaultProps;

export default UserList;
