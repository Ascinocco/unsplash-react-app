import { createSelector } from 'reselect';

const selector = state => state.userReducer.userList;
const list = (results = []) => Object.values(results);

// Maps availabilities state to an array of availabilities
export default createSelector(
  selector,
  list,
);
