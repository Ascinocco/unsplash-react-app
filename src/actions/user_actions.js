import axios from 'axios';
import { CLIENT_ID } from '../config';

export const SEARCH_USERS = 'LIST_IMAGES';

export function searchUsers(userName, dispatch, successCallback, failureCallback) {
  const url = `https://api.unsplash.com/users/${userName}/?client_id=${CLIENT_ID}`;
  axios.get(url).then((res) => {
    dispatch({ type: SEARCH_USERS, payload: res, errors: null });
    successCallback()
  }).catch((err) => {
    dispatch({ type: SEARCH_USERS, payload: err, errors: true });
    failureCallback()
  });
};
