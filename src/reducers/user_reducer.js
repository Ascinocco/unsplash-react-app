import isEmpty from 'lodash.isempty';
import { SEARCH_USERS } from '../actions/user_actions';

export default (state = {}, action) => {
  switch(action.type) {
    case SEARCH_USERS:
      const { data = {} } = action.payload;
      const { userList = [] } = state;
      let updatedUserList = userList;
      if (!isEmpty(data)) {
        const userAlreadyExists = userList.find(user => user.id === data.id);
        updatedUserList = userAlreadyExists ? userList : [...userList, data];
      }

      return { ...state, userList: updatedUserList};
    default:
      return state;
  }
}
