import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { searchUsers } from '../../actions/user_actions';
import UserListSelector from '../../selectors/user_list_selector';

import Search from '../../components/Search';
import UserList from '../../components/UserList';
import PhotoGrid from '../../components/PhotoGrid'

import styles from './Home.module.css';

const propTypes = {
  onSearch: PropTypes.func.isRequired,
  userList: PropTypes.array,
};

const defaultProps = {
  userList: [],
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      userName: '',
      photoGridImages: [],
      errors: {
        userNotFound: undefined,
      }
    };
  }

  render(){
    return (
      <div className={styles['home-container']}>
        <div className="row">
          <div className="col-sm-2 col-md-2 col-lg-2">
            <Search
              userNotFoundError={this.state.errors.userNotFound}
              onChangeInput={(e) => this.setState({ userName: e.target.value || '' })}
              onClickSearch={() => {
                this.setState({ errors: { userNotFound: undefined } }, () => {
                  const successCallback = () => this.setState({ userName: '' });
                  const failureCallback = () => this.setState({ errors: { userNotFound: true } });
                  this.props.onSearch(
                    this.state.userName,
                    successCallback,
                    failureCallback,
                  )
                })
              }}
              value={this.state.userName}
            />
            <UserList
              currentlySelectedUser={this.state.currentlySelectedUser}
              userList={this.props.userList}
              onSelectUser={(user) => this.setState({ photoGridImages: user.photos, currentlySelectedUser: user })}
            />
          </div>
          <div className="col-sm-6 col-md-6 col-lg-6">
            <PhotoGrid
              currentlySelectedUser={this.state.currentlySelectedUser}
              photoGridImages={this.state.photoGridImages}
            />
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = propTypes;
Home.defaultProps = defaultProps;

const mapDispatchToProps = dispatch => ({
  onSearch: (userName, successCallback, failureCallback) => {
    return searchUsers(
      userName,
      dispatch,
      successCallback,
      failureCallback,
    );
  },
});

const mapStateToProps = state => ({
  userList: UserListSelector(state) || [],
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
